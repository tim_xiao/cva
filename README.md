For years, a widespread practice in the industry has been to mark derivative portfolios to market without taking counterparty risk into account. All cash flows are discounted using the LIBOR curve. But the real parties, in many cases, happen to be of lower credit quality than the hypothetical LIBOR party and have a chance of default.

As a consequence, the International Accounting Standard (IAS) 39 requires banks to provide a fair-value adjustment due to counterparty risk. Although credit value adjustment (CVA) became mandatory in 2000, it received a little attention until the recent financial crises in which the profit and loss (P&L) swings due to CVA changes were measured in billons of dollars. Interest in CVA began to grow. Now CVA has become the first line of defense and the central part of counterparty risk management.

CVA not only allows institutions to move beyond the traditional control mindset of credit risk limits and to quantify counterparty risk as a single measurable P&L number, but also offers an opportunity for banks to dynamically manage, price and hedge counterparty risk. The benefits of CVA are widely acknowledged. Many banks have set up internal credit risk trading desks to manage counterparty risk on derivatives.

The earlier works on CVA are mainly focused on unilateral CVA that assumes that only one counterparty is defaultable and the other one is default-free. The unilateral treatment neglects the fact that both counterparties may default, i.e., counterparty risk can be bilateral. A trend that has become increasingly relevant and popular has been to consider the bilateral nature of counterparty credit risk. Although most institutions view bilateral considerations as important in order to agree on new transactions, Hull and White (2013) argue that bilateral CVA is more controversial than unilateral CVA as the possibility that a dealer might default is in theory a benefit to the dealer.

CVA, by definition, is the difference between the risk-free portfolio value and the true (or risky or defaultable) portfolio value that takes into account the possibility of a counterparty’s default. The risk-free portfolio value is what brokers quote or what trading systems or models normally report. The risky portfolio value, however, is a relatively less explored and less transparent area, which is the main challenge and core theme for CVA. In other words, central to CVA is risky valuation.

In general, risky valuation can be classified into two categories: the default time approach (DTA) and the default probability approach (DPA). The DTA involves the default time explicitly. Most CVA models in the literature (Brigo and Capponi (2008), Lipton and Sepp (2009), Pykhtin and Zhu (2006) and Gregory (2009), etc.) are based on this approach. 

Although the DTA is very intuitive, it has the disadvantage that it explicitly involves the default time. We are very unlikely to have complete information about a firm’s default point, which is often inaccessible (see Duffie and Huang (1996), Jarrow and Protter (2004), etc.). Usually, valuation under the DTA is performed via Monte Carlo simulation. On the other hand, however, the DPA relies on the probability distribution of the default time rather than the default time itself. Sometimes the DPA yields simple closed form solutions.

The current popular CVA methodology (Pykhtin and Zhu (2006) and Gregory (2009), etc.) is first derived using DTA and then discretized over a time grid in order to yield a feasible solution. The discretization, however, is inaccurate. In fact, this model has never been rigorously proved. Since CVA is used for financial accounting and pricing, its accuracy is essential. Moreover, this current model is based on a well-known assumption, in which credit exposure and counterparty’s credit quality are independent. Obviously, it can not capture wrong/right way risk properly. 

In this paper, we present a framework for risky valuation and CVA. In contrast to previous studies, the model relies on the DPA rather than the DTA. Our study shows that the pricing process of a defaultable contract normally has a backward recursive nature if its payoff could be positive or negative. 

An intuitive way of understanding these backward recursive behaviours is that we can think of that any contingent claim embeds two default options. In other words, when entering an OTC derivatives transaction, one party grants the other party an option to default and, at the same time, also receives an option to default itself. In theory, default may occur at any time. Therefore, the default options are American style options that normally require a backward induction valuation.

Wrong way risk occurs when exposure to a counterparty is adversely correlated with the credit quality of that counterparty, while right way risk occurs when exposure to a counterparty is positively correlated with the credit quality of that counterparty. For example, in wrong way risk exposure tends to increase when counterparty credit quality worsens, while in right way risk exposure tends to decrease when counterparty credit quality declines. Wrong/right way risk, as an additional source of risk, is rightly of concern to banks and regulators. Since this new model allows us to incorporate correlated and potentially simultaneous defaults into risky valuation, it can naturally capture wrong/right way risk.

This article presents a framework for pricing risky contracts and their CVAs. The model relies on the probability distribution of the default jump rather than the default jump itself, because the default jump is normally inaccessible. We find that the valuation of risky assets and their CVAs, in most situations, has a backward recursive nature and requires a backward induction valuation. An intuitive explanation is that two counterparties implicitly sell each other an option to default when entering into an OTC derivative transaction. If we assume that a default may occur at any time, the default options are American style options. If we assume that a default may only happen on the payment dates, the default options are Bermudan style options. Both Bermudan and American options require backward induction valuations. 

Based on our theory, we propose a novel cash-flow-based framework (see appendix) for calculating bilateral CVA at the counterparty portfolio level. This framework can easily incorporate various credit mitigation techniques, such as netting agreements and margin agreements, and can capture wrong/right way risk. Numerical results show that these credit mitigation techniques and wrong/right way risk have significant impacts on CVA. 

Reference

Brigo, D., and Capponi, A., 2008, Bilateral counterparty risk valuation with stochastic dynamical models and application to Credit Default Swaps, Working paper.

Duffie, Darrell, and Ming Huang, 1996, Swap rates and credit quality, Journal of Finance, 51, 921-949.

Duffie, Darrell, and Kenneth J. Singleton, 1999, Modeling term structure of defaultable bonds, Review of Financial Studies, 12, 687-720.

FinPricing, 2015, Market Data, https://finpricing.com/lib/IrCurveIntroduction.html

Gregory, Jon, 2009, Being two-faced over counterparty credit risk, RISK, 22, 86-90.

Hull, J. and White, A., 2013, CVA and wrong way risk, forthcoming, Financial Analysts Journal.

Jarrow, R. A., and Protter, P., 2004, Structural versus reduced form models: a new information based perspective, Journal of Investment Management, 2, 34-43.

Jarrow, Robert A., and Stuart M. Turnbull, 1995, Pricing derivatives on financial securities subject to credit risk, Journal of Finance, 50, 53-85.

Lipton, A., and Sepp, A., 2009, Credit value adjustment for credit default swaps via the structural default model, Journal of Credit Risk, 5(2), 123-146.

Longstaff, Francis A., and Eduardo S. Schwartz, 2001, Valuing American options by simulation: a simple least-squares approach, The Review of Financial Studies, 14 (1), 113-147.

Moody’s Investor’s Service, 2000, Historical default rates of corporate bond issuers, 1920-99.

J. P. Morgan, 1999, The J. P. Morgan guide to credit derivatives, Risk Publications.

O’Kane, D. and S. Turnbull, 2003, Valuation of credit default swaps, Fixed Income Quantitative Credit Research, Lehman Brothers, QCR Quarterly, 2003 Q1/Q2, 1-19.

Pykhtin, Michael, and Steven Zhu, 2007, A guide to modeling counterparty credit risk, GARP Risk Review, July/August, 16-22.

Sorensen, E. and T. Bollier, 1994, Pricing swap default risk, Financial Analysts Journal, 50, 23-33.

Xiao, T., 2013a, The impact of default dependency and collateralization on asset pricing and credit risk modeling, Working paper.

Xiao, T., 2013b, An economic examination of collateralization in different financial market, Working paper.


